%define node 0

%macro colon 2
	%ifstr %1
		%ifid %2
			%2: dq node, .key, .value
			.key: db %1, 0
			.value:
			
			%define node %2
		%else
			%error "Second value must be a label"
		%endif
	%else
		%error "First value must be a string type"
	%endif
%endmacro
