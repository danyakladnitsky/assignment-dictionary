global string_length
global string_copy
global print_error
global read_string
global print_string
global print_char
global print_int
global string_equals
global read_char
global read_word
global print_newline
global print_uint

global parse_uint
global parse_int
global exit


section .data

%define SYS_READ 0
%define SYS_WRITE 1

%define STDIN_CODE 0
%define STDOUT_CODE 1

%define EXIT_CODE 60

%define NEW_LINE '\n'
%define TAB_SYMBOL 9


section .text
 
 
exit: 
    mov rax, EXIT_CODE
    syscall


string_length:
    xor rax, rax
.loop:
    cmp byte[rdi + rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret


print_string:
    push rdi
    call string_length
    pop rsi
    push rax
    
    mov rdx, rax
    mov rax, SYS_WRITE
    mov rdi, STDOUT_CODE
    syscall
    
    pop rax
    ret

print_char:
    push rdi	
    mov rdi, rsp
    call print_string
    pop rdi	
    ret

print_newline:
    mov rdi, NEW_LINE

read_string:
	push rdi
	dec rsi
	mov rdx, rsi
	mov rsi, rdi
	mov rax, 0
	mov rdi, 0
	syscall
	pop rdi
	cmp rax, 0
	jl .error
	lea rdx, [rdi + rax]
	mov rax, rdi
	dec rdx
	cmp [rdx], byte 10
	jne .no_lf
	mov [rdx], byte 0
	ret
	
.no_lf:
	inc rdx
	mov [rdx], byte 0
	ret

.error:
	mov rax, 0
	ret



print_uint:
    .start:
        push rax
        push rdx
        mov rax, rdi
        mov rdi, 10
        push 0x0
    .integer_to_string_convert:
    	xor rdx, rdx
    	div rdi
    	add rdx, '0'
    	push rdx
    	cmp rax, 0
    	je .print_numeric_value
    	jmp .integer_to_string_convert
    .print_numeric_value:
    	pop rdi
    	cmp rdi, 0x0
    	je .end
    	call print_char
    	jmp .print_numeric_value
    .end:
        pop rdx
        pop rax
    	ret

print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
	jmp print_uint


string_equals:
    mov al, byte[rdi]
    cmp al, byte[rsi]
    jne .various
    inc rdi
    inc rsi
    test al, al
    jne string_equals
    mov rax, 1
    ret
.various:
    xor rax, rax
    ret


read_char:
	push 0
	mov rax, SYS_READ	
	mov rdi, STDIN_CODE
	mov rsi, rsp		
	mov rdx, 1	
	syscall	
	xor rax, rax	
	pop rax	
	ret

read_word:
	mov r9, rsi
	push rdi
	push rsi

.space_loop:
	call read_char
	cmp rax, 0x9
	je .space_loop
	cmp rax, 0xA
	je .space_loop
	cmp rax, 0x20
	je .space_loop

	pop rsi
	pop rdi
	xor r8, r8
	
.symbol_loop:
	cmp rax, 0
	jz .break
	cmp rax, 0x9
	je .break
	cmp rax, 0xA
	je .break
	cmp rax, 0x20
	je .break
	cmp r8, r9
	jae .over
	mov [rdi+r8], rax
	inc r8
	push rdi
	push rsi
	call read_char
	pop rsi
	pop rdi
	jmp .symbol_loop

.break:
	mov rax, rdi
	mov rdx, r8
	ret

.over:
	xor rax, rax
	ret
 


parse_uint:
    xor rax, rax
    mov r8, 10
    xor rcx, rcx
    xor r9, r9
.loop:
    mov r9b, byte [rdi + rcx]
    cmp r9b, '0'
    jb .end
    cmp r9b, '9'
    ja .end
    mul r8
    sub r9b, '0'
    add rax, r9
    inc rcx
    jmp .loop
    ret
.end:
    mov rdx, rcx
    ret

parse_int:
	cmp byte[rdi], '-'
	je .neg
	call parse_uint
	cmp rdx, 0
	je .end
	ret

.neg:
	inc rdi
	call parse_uint
	cmp rdx, 0
	je .end
	neg rax
	inc rdx
	ret

.end:
	xor rax, rax
	xor rdx, rdx
	ret

string_copy:
	call string_length
	inc rax
	cmp rax, rdx
	ja .error
	mov r8, rax
	dec rdi
	dec rsi
	xor rcx, rcx

.loop:
	cmp rcx, r8
	je .break
	inc rdi
	inc rsi
	mov r9b, byte[rdi]
	mov byte[rsi], r9b
	inc rcx
	jmp .loop

.break:
	mov rax, r8
	ret

.error:
	xor rax, rax
	ret


print_error:
	mov rsi, 2
	jmp print_string

