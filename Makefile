ASM_REQUIRED_FLAGS=-f elf64


main: dict.o lib.o main.o 
	ld -o $@ $^
main.o: main.asm colon.inc words.inc lib.inc 
	nasm $(ASM_REQUIRED_FLAGS) -o $@ $<
%.o: %.asm
	nasm $(ASM_REQUIRED_FLAGS) -o $@ $<


clear:
	rm *.o; rm main
