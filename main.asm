
; imports
%include 'lib.inc'
%include 'colon.inc'
%include 'words.inc'

; exports
extern find_word

section .rodata
; constants
THROW_ERRROR: db 'Oopsie. 0_0. Something bad happened', 10, 0
NO_KEY_ERROR: db 'Your key is invalid. Check spelling. There is no such key in dictionary', 10, 0

; entry point
global _start
section .text
_start:
	sub rsp, 256
	mov rdi, rsp
	mov rsi, 256

	call read_string
	test rax, rax
	je .input_err
	mov rdi, rsp
	mov rsi, node

	call find_word
	add rsp, 256
	test rax, rax
    
    ; handle errors
	je .THROW_NO_KEY_ERROR
	mov rdi, [rax + 16]

    ; render value
	call print_string
	call print_newline
	call exit	
	
.input_err:
	add rsp, 256
	mov rdi, THROW_ERRROR
	call print_error
	call exit
	
.THROW_NO_KEY_ERROR:
	mov rdi, NO_KEY_ERROR
	call print_error
	call exit
